{
  description = "Generate Steeplechase relationship graph based on Data from... somewhere.";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs";
    flake-utils.url = "github:numtide/flake-utils";
  };

  # https://nixos.wiki/wiki/Flakes#Output_schema
  # https://xeiaso.net/blog/nix-flakes-1-2022-02-21
  outputs = { self, nixpkgs, flake-utils }:
   # https://github.com/numtide/flake-utils#eachdefaultsystem---system---attrs
   flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = import nixpkgs { inherit system; };
      in
      rec {
        defaultPackage = with pkgs; stdenv.mkDerivation {
          # This is the name of the excutable that we look for when we `nix run`.
          # You can think of it as kind of the entry point for `nix run`. It's
          # an `.sh`, so we must keep .sh on it.
          name = "steeplechase.sh";

          # Deps we need to know about at build
          buildInputs = [
            graphviz
          ];

          # Where to look for our working files. usually the current directory.
          # Could be elsewhere.
          src = ./.;

          # This we need to do to files before we give them a home; in this case
          # We're creating a shell script; that knows about the locaation of 
          # graphviz and its cute babies; like dot. Making sure it can run.
          # The "extract names" thing could be here.
          buildPhase = ''
            echo '${pkgs.graphviz}/bin/dot -Tpng -Gdpi=300 -o "$1" ./characters.dot' > steeplechase.sh
            chmod +x steeplechase.sh
          '';

          # Where we want to install the files when it's put into the nix-store.
          # By default; nix run looks for executable in 
          # `(someDerivationPath)/bin/` directory of our derivation. We also
          # need to symlink the folder of Graphviz. Otherwise; how would it 
          # know where to look?

          # I am also lazy, and decided to put the source data file in the
          # `bin` directory. I could go some place more appropreate.

          # Note: `$out` is just a placeholder for the final derivation path
          # within the nix store.

          # https://nixos.wiki/wiki/Nix_Cookbook#Wrapping_packages
          # Talks about options for wrapping packages, which is *kind of* what
          # we're doing here.
          installPhase = ''
            mkdir -p $out/bin
            ln -s ${pkgs.graphviz}
            cp steeplechase.sh $out/bin/
            cp characters.dot $out/bin/
          '';
        };

        # `apps`; This is what tells `nix run` that things can be treated like 
        # an application. I needs to point to the package we made ^^up there^^.
        # https://determinate.systems/posts/nix-run
        # https://nixos.wiki/wiki/Flakes#nix_run
        # https://nixos.org/manual/nix/stable/command-ref/new-cli/nix3-run.html
        # https://github.com/numtide/flake-utils#mkapp--drv-name--drvpname-or-drvname-exepath--drvpassthruexepath-or-binname
        apps = flake-utils.lib.mkApp { drv = defaultPackage; };
    });
}
