# _The Adventure Zone: Steeplechase_ Character Map

A map of most of the named characters in _The Adventure Zone: Steeplechase_.

if you have nix package manager installed, and flakes are enabled; you can just run: 

```
nix run gitlab:cbleslie/steeple-chase-graph-nix-flake-example# ~/Desktop/newtest.png
```
